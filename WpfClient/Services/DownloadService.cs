using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MyDrive.Abstractions;
using MyDrive.GoogleServices;
using MyDrive.LocalServices;

namespace MyDrive.WpfClient.Services
{
    internal class DownloadController
    {
        private (string fullPath, string folder) GetLocalPaths(string remoteFileName, IFile selectedDestination)
        {
            var localFolder = selectedDestination.IsFolder
                ? selectedDestination.Id
                : Path.GetDirectoryName(selectedDestination.Id);

            var localPath = Path.Combine(localFolder, remoteFileName);

            return (fullPath: localPath, folder: localFolder);
        }


        public async Task DonwloadFileAsync(Node<IFile> source, Node<IFile> destination)
        {
            var service = await ServiceProvider.GetNewFilesService();
            var paths = GetLocalPaths(source.Value.Name, destination.Value);

            using (var fileStream = File.OpenWrite(paths.fullPath))
            {
                await service.DownloadFileAsync(source.Value, fileStream);
            }
        }

        public async Task DownloadFolderAsync(Action<int> progress, Node<IFile> source, Node<IFile> destination)
        {
            var service = await ServiceProvider.GetNewFilesService();

            var children = (await service.GetChildrenAsync(source)).ToArray();

            var localFolder = destination.Value.IsFolder
                ? destination.Value.Id
                : Path.GetDirectoryName(destination.Value.Id);

            var localFolderHasDifferentName = !Path.GetDirectoryName(localFolder).Equals(source.Value.Name);
            if (localFolderHasDifferentName)
            {
                var innerFolder = Directory.GetDirectories(localFolder, source.Value.Name).FirstOrDefault();
                if (innerFolder == null)
                {
                    var newDirectory = Directory.CreateDirectory(Path.Combine(localFolder, source.Value.Name));
                    localFolder = newDirectory.FullName;
                }
                else
                {
                    localFolder = innerFolder;
                }
            }

            var onePercent = (float)children.Length / 100;

            for (var i = 0; i < children.Length; i++)
            {
                progress((int)(i / onePercent));
                var child = children[i];

                if (child.Value.IsFolder)
                {
                    await DownloadFolderAsync(progress, child,
                        new Node<IFile>
                        {
                            Value = new LocalFile { FullPath = localFolder, IsFolder = true, Name = child.Value.Name }
                        });
                }
                else
                {
                    var localPath = Path.Combine(localFolder, child.Value.Name);
                    using (var fileStream = File.OpenWrite(localPath))
                    {
                        await service.DownloadFileAsync(child.Value, fileStream);
                    }
                }

                
            }
        }
    }
}