﻿using System.Windows;
using FolderSelect;

namespace MyDrive.WpfClient.Services
{
    interface IFileDialogService
    {
        string ShowFileDialog();
    }

    internal class FileDialogService : IFileDialogService
    {
        public string ShowFileDialog()
        {
            var result = string.Empty;

            // TODO: Do I really need an Invoke here?
            Application.Current.MainWindow.Dispatcher.Invoke(() =>
            {
                var dialog = new FolderSelectDialog
                {
                    InitialDirectory = LocalServices.FilesService.LocalRootFolder,
                    Title = "Select a root folder for",
                };

                var folderSelected = dialog.ShowDialog();
                result = folderSelected ? dialog.FileName : string.Empty;
            });

            return result;
        }
    }
}
