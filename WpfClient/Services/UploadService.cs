using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MyDrive.Abstractions;
using MyDrive.GoogleServices;
using MyDrive.LocalServices;
using FilesService = MyDrive.GoogleServices.FilesService;

namespace MyDrive.WpfClient.Services
{
    internal class UploadController
    {
        // https://stackoverflow.com/questions/1029740/get-mime-type-from-filename-extension
        internal string GetMimeType(string fileName)
        {
            var mimeType = string.Empty; //"application/unknown";
            var ext = Path.GetExtension(fileName).ToLower();
            var regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (regKey?.GetValue("Content Type") != null)
            {
                mimeType = regKey.GetValue("Content Type").ToString();
            }

            return mimeType;
        }

        /// <param name="updateProgress">Should use Dispatcher.Invoke in case progress value will be used to update Progress Bar (or any UI work is to be done)</param>
        public async Task UploadFile(Action<long> updateProgress, string destinationParentId, IFile source)
        {
            var service = await ServiceProvider.GetNewFilesService();
            
            using (var stream = File.OpenRead(source.Id))
            {
                /*  Inside Google's UploadAsync method, .ConfigureAwait(false) is used.
                 *  As a result, the first Progress Update call, that goes before ConfigureAwait, 
                 *  will be executed in Main thread (UI), and everything will be fine. But other 
                 *  Progress Updates will go after the ConfigureAwait(false) - so thread context
                 *  will be lost and I'll get an exception about trying to access UI object
                 *  from another thread. Thus, updateProgress() should use MainWindow.Dispatcher.Invoke
                 *  in case UI element should be accessed - like a Progress Bar. I can move MainWindow.Dispatcher.Invoke
                 *  call here, but that would make it impossible to get rid of MainWindow reference.
                 */
                void OnUploadProgressChanged(object obj, long percentage)
                {
                    updateProgress(percentage);
                }

                void OnUploadFinished(object obj, EventArgs args)
                {
                    service.UploadProgressChanged -= OnUploadProgressChanged;
                    service.UploadFinished -= OnUploadFinished;
                }

                service.UploadProgressChanged += OnUploadProgressChanged;
                service.UploadFinished += OnUploadFinished;

                await service.UploadFileAsync(source.Name, stream, GetMimeType(source.Id), destinationParentId);
            }

        }

        /// <summary>
        /// Uploads files with source and destination validation.
        /// Doesn't check whether any files are present in the destination or not.
        /// If uploadFrom and uploadTo are folders with equal names - will upload content from one to another.
        /// If uploadFrom is not a folder - will upload content of it's parent.
        /// If uploadTp is not a folder, or has a different name - will create a new folder for upload under uploadTo parent.
        /// </summary>
        /// <param name="uploadFrom"></param>
        /// <param name="uploadTo"></param>
        /// <param name="uploadSubfolders">Set True if want to all source content recursively</param>
        public async Task UploadFolder(Node<IFile> uploadFrom, Node<IFile> uploadTo, bool uploadSubfolders = false)
        {
            var remoteService = await ServiceProvider.GetNewFilesService();
            
            if (!uploadFrom.Value.IsFolder)
            {
                uploadFrom = uploadFrom.Parent;
            }

            var localFolder = uploadFrom.Value;

            uploadTo = await GetRemoteFolderForUpload(uploadFrom, uploadTo, remoteService);

            await UploadDirectoryFiles(localFolder, uploadTo.Value, remoteService);

            if (uploadSubfolders)
            {
                await UploadDirectoryFolders(uploadFrom, uploadTo, remoteService);
            }
        }

        private async Task UploadDirectoryFiles(IFile uploadFrom, IFile uploadTo, FilesService remoteService)
        {
            var localFiles = Directory.GetFiles(uploadFrom.Id);

            foreach (var filePath in localFiles)
            {
                using (var stream = File.OpenRead(filePath))
                {
                    await remoteService.UploadFileAsync(Path.GetFileName(filePath), stream, GetMimeType(filePath), uploadTo.Id);
                }
            }
        }

        private async Task UploadDirectoryFolders(Node<IFile> uploadFrom, Node<IFile> uploadTo, FilesService remoteService)
        {
            var localSubfolders = Directory.GetDirectories(uploadFrom.Value.Id).Select(x => new LocalFile {FullPath = x, IsFolder = true, Name = Path.GetFileName(x)});
            foreach (var localSubfolder in localSubfolders)
            {
                var remoteSubfolder = await remoteService.CreateFolderAsync(localSubfolder.Name, uploadTo.Value.Id);
                var localNode = new Node<IFile> { Value = localSubfolder, Parent = uploadFrom };
                var remoteNode = new Node<IFile>
                {
                    Value = remoteSubfolder,
                    Parent = uploadTo,
                };
                await UploadFolder(localNode, remoteNode, true);
            }
        }

        /// <summary>
        /// Checks if current <c>destination</c> is a folder and it's name is the same as of <c>source</c>.
        /// Otherwise creates such a folder under a <c>destination</c> parent (i.e. <c>destination</c> sibling).
        /// </summary>
        /// <param name="source"></param>
        /// <param name="destination"></param>
        /// <param name="service"></param>
        /// <returns>Either a <c>destination</c> or a new folder</returns>
        private static async Task<Node<IFile>> GetRemoteFolderForUpload(Node<IFile> source, Node<IFile> destination, FilesService service)
        {
            var remoteFile = destination.Value;
            var localFolder = source.Value;

            var namesAreSimilar = (remoteFile.Name?.Equals(localFolder.Name, StringComparison.OrdinalIgnoreCase)).GetValueOrDefault(false);

            if (namesAreSimilar && remoteFile.IsFolder)
            {
                return destination;
            }

            var newRemoteFolder = await service.CreateFolderAsync(localFolder.Name, destination.Parent?.Value.Id ?? destination.Value.Id); // root has a NULL parent
            return new Node<IFile>
            {
                Value = newRemoteFolder,
                Parent = destination,
            };
        }
    }
}