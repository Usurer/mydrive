﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using MyDrive.Abstractions;
using MyDrive.GoogleServices;
using MyDrive.LocalServices;

namespace MyDrive.WpfClient.ViewModels
{
    public class NodeViewModel : ViewModelBase
    {
        private IFile _value;
        private NodeViewModel _parent;
        private ObservableCollection<NodeViewModel> _children = new ObservableCollection<NodeViewModel>();
        private bool _isNodeExpanded;

        public Node<IFile> Model { get; set; }

        public IFile Value
        {
            get => _value;
            set
            {
                _value = value;
                OnPropertyChanged(nameof(Value));
            }
        }

        public NodeViewModel Parent
        {
            get => _parent;
            set
            {
                _parent = value;
                OnPropertyChanged(nameof(Parent));
            }
        }

        public ObservableCollection<NodeViewModel> Children
        {
            get => _children;
            set
            {
                _children = value;
                OnPropertyChanged(nameof(Children));
            }
        }

        public NodeViewModel FromModel(Node<IFile> model, NodeViewModel parent)
        {
            Model = model;
            Value = model.Value;
            var children = model.Children.Select(x =>
            {
                var m = new NodeViewModel();
                m = m.FromModel(x, this);
                return m;
            });
            Children = new ObservableCollection<NodeViewModel>(children);
            Parent = parent;
            return this;
        }

        public bool IsNodeExpanded
        {
            get => _isNodeExpanded;
            set
            {
                _isNodeExpanded = value;
                OnPropertyChanged(nameof(IsNodeExpanded));
            }
        }

        public string FullPath
        {
            get => GetFullPath();
        }

        public ICommand LoadContents { get; set; }

        public List<NodeViewModel> GetAllParents()
        {
            var result = new List<NodeViewModel>();
            var item = this;

            while (item.Parent != null)
            {
                result.Add(item.Parent);
                item = item.Parent;
            }

            return result;
        }

        public string GetFullPath()
        {
            if (Value is LocalFile)
            {
                return (Value as LocalFile).FullPath;
            }
            if (Value is RemoteFile)
            {
                var parents = GetAllParents().Select(x => ((RemoteFile)x.Value).Name).ToArray();
                parents = parents.Reverse().ToArray();
                parents[0] = "Google Drive:";
                return string.Join("\\", parents.Concat(new []{ Value.Name }));
            }

            throw new NotImplementedException();
        }
    }
}
