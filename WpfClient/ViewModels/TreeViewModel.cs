﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using MyDrive.Abstractions;
using MyDrive.WpfClient.MVVM;

namespace MyDrive.WpfClient.ViewModels
{
    public abstract class TreeViewModel : TreeViewModelBase
    {
        internal abstract IFilesService FilesService { get; set; }

        private Visibility _visibility = Visibility.Visible;
        public NodeViewModel _rootNode { get; set; }

        public Visibility Visibility
        {
            get => _visibility;
            set
            {
                _visibility = value;
                OnPropertyChanged(nameof(Visibility));
            }
        }

        public NodeViewModel RootNode
        {
            get => _rootNode;
            set
            {
                _rootNode = value;
                OnPropertyChanged(nameof(RootNode));
            }
        }

        public abstract Task LoadTreeNodeAsync(NodeViewModel nodeViewModel);

        protected async Task LoadChildren(NodeViewModel file)
        {
            if (!file.Value.IsFolder || file.Children.Any())
            {
                return;
            }

            var children = await FilesService.GetChildrenAsync(file.Model);
            file.Children = new ObservableCollection<NodeViewModel>(children.Select(x =>
            {
                var val = new NodeViewModel().FromModel(x, file);
                val.LoadContents = new RelayCommand(async param => await LoadTreeNodeAsync((NodeViewModel)param));
                return val;
            }));
        }

        protected async Task<NodeViewModel> OpenInSync(NodeViewModel sourceNode, NodeViewModel projectionNode)
        {
            var sourceParents = sourceNode.GetAllParents().ToArray();
            var projectionParents = projectionNode.GetAllParents();
            var projectionRoot = projectionParents.Any() ? projectionParents.Last() : projectionNode;
            var projectionRootChildren = projectionRoot.Children.ToList();

            // If source node is the root child - let's just search for another node under projection root - no tree traverse needed
            if (sourceParents.Length > 1)
            {
                // start from the parent next to the root and go from the end of the list to it's beginning - i.e. from the topmost levels to the closest parent
                for (var i = sourceParents.Length - 2; i >= 0; i--)
                {
                    var projectionParent = sourceParents[i];
                    var projectionNodeFounded = projectionRootChildren.Find(x => x.Value.Name.Equals(projectionParent.Value.Name, StringComparison.Ordinal) && x.Value.IsFolder);
                    if (projectionNodeFounded == null)
                    {
                        return null;
                    }

                    projectionNodeFounded.IsNodeExpanded = true;

                    if (projectionNodeFounded.Children.Count == 0)
                    {
                        var contents = await FilesService.GetChildrenAsync(projectionNodeFounded.Model);

                        projectionNodeFounded.Children = new ObservableCollection<NodeViewModel>(contents.Select(x => new NodeViewModel().FromModel(x, projectionNodeFounded)));
                    }

                    projectionRootChildren = projectionNodeFounded.Children.ToList();
                }
            }
            return projectionRootChildren.Find(x => x.Value.Name.Equals(sourceNode.Value.Name, StringComparison.Ordinal));
        }
    }
}
