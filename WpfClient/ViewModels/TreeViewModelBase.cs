﻿using System;

namespace MyDrive.WpfClient.ViewModels
{
    public abstract class TreeViewModelBase : ViewModelBase
    {
        public class SelectedItemChangedEventArgs : EventArgs
        {
            public NodeViewModel NewValue { get; set; }
        }

        private NodeViewModel _selectedNode { get; set; }

        public EventHandler<SelectedItemChangedEventArgs> SelectedItemChanged;

        public NodeViewModel SelectedNode
        {
            get => _selectedNode;
            set
            {
                // This check is important, because SelectedItemChanged may lead to further changes of selected node and it all leads to Stack Overflow.
                // And the check is good anyway.
                if (_selectedNode != value)
                {
                    _selectedNode = value;
                    OnPropertyChanged(nameof(SelectedNode));
                    SelectedItemChanged?.Invoke(this, new SelectedItemChangedEventArgs { NewValue = value });
                }
            }
        }

    }
}
