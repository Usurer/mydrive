﻿using System.Linq;
using System.Threading.Tasks;
using MyDrive.Abstractions;
using MyDrive.LocalServices;

namespace MyDrive.WpfClient.ViewModels
{
    public class LocalTreeViewModel : TreeViewModel
    {
        internal static string LocalRoot = @"C:\";

        internal override IFilesService FilesService { get; set; }

        public ProgressViewModel Progress { get; set; } = new ProgressViewModel();

        private async Task LoadChildrenToModel(NodeViewModel file)
        {
            if (FilesService == null)
            {
                FilesService = new FilesService();
            }

            await LoadChildren(file);
        }

        public override async Task LoadTreeNodeAsync(NodeViewModel nodeViewModel)
        {
            await LoadChildrenToModel(nodeViewModel);
            nodeViewModel.IsNodeExpanded = true;
        }

        public void LoadRoot()
        {
            var root = new Node<IFile> { Value = new LocalFile { FullPath = LocalRoot, IsFolder = true } };
            RootNode = new NodeViewModel().FromModel(root, null);

            LoadFolderContents(RootNode);
            if (RootNode.Children.Any())
            {
                SelectedNode = RootNode.Children.First();
            }
        }

        private async void LoadFolderContents(NodeViewModel file)
        {
            await LoadChildrenToModel(file);
        }

        public async Task OpenInSync(NodeViewModel nodeToSyncWith)
        {
            var localFound = await base.OpenInSync(nodeToSyncWith, SelectedNode ?? RootNode);
            if (localFound != null)
            {
                SelectedNode = localFound;
            }
        }
    }
}
