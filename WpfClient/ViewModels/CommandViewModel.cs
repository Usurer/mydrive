﻿using System;
using System.Windows.Input;

namespace MyDrive.WpfClient.ViewModels
{
    class CommandViewModel : ViewModelBase
    {
        public CommandViewModel(string displayName, ICommand command)
        {
            Command = command ?? throw new ArgumentNullException(nameof(command));
        }

        public ICommand Command
        {
            get;
            private set;
        }
    }
}
