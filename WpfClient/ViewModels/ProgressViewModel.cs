﻿using System.Windows;

namespace MyDrive.WpfClient.ViewModels
{
    public class ProgressViewModel : ViewModelBase
    {
        private int _value;
        private bool _isIndeterminate = true;
        private Visibility _visibility = Visibility.Collapsed;
        private int _maximum = 100;

        public int Value
        {
            get => _value;
            set
            {
                _value = value;
                OnPropertyChanged(nameof(Value));
            }
        }

        public bool IsIndeterminate
        {
            get => _isIndeterminate;
            set
            {
                _isIndeterminate = value;
                OnPropertyChanged(nameof(IsIndeterminate));
            }
        }

        public Visibility Visibility
        {
            get => _visibility;
            set
            {
                _visibility = value;
                OnPropertyChanged(nameof(Visibility));
            }
        }

        public int Maximum
        {
            get => _maximum;
            set
            {
                _maximum = value;
                OnPropertyChanged(nameof(Maximum));
            }
        }
    }
}
