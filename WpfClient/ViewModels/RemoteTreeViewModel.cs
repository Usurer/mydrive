﻿using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using MyDrive.Abstractions;
using MyDrive.GoogleServices;

namespace MyDrive.WpfClient.ViewModels
{
    public class RemoteTreeViewModel : TreeViewModel
    {
        internal override IFilesService FilesService { get; set; }

        public ProgressViewModel Progress { get; set; } = new ProgressViewModel();

        private async Task LoadChildrenToModel(NodeViewModel file)
        {
            if (FilesService == null)
            {
                FilesService = await ServiceProvider.GetNewFilesService();
            }

            Visibility = Visibility.Collapsed;
            Progress.Visibility = Visibility.Visible;

            await LoadChildren(file);

            Progress.Visibility = Visibility.Collapsed;
            Visibility = Visibility.Visible;
        }

        public override async Task LoadTreeNodeAsync(NodeViewModel nodeViewModel)
        {
            await LoadChildrenToModel(nodeViewModel);
            nodeViewModel.IsNodeExpanded = true;
        }

        public async Task LoadRoot()
        {
            var root = new Node<IFile> { Value = new RemoteFile { IsFolder = true, Id = "root" } };
            RootNode = new NodeViewModel().FromModel(root, null);

            await LoadChildrenToModel(RootNode);
            if (RootNode.Children.Any())
            {
                SelectedNode = RootNode.Children.First();
            }
        }

        public async Task OpenInSync(NodeViewModel nodeToSyncWith)
        {
            Visibility = Visibility.Hidden;
            Progress.Visibility = Visibility.Visible;

            var remoteFound = await base.OpenInSync(nodeToSyncWith, SelectedNode ?? RootNode);

            if (remoteFound != null)
            {
                SelectedNode = remoteFound;
            }

            Progress.Visibility = Visibility.Collapsed;
            Visibility = Visibility.Visible;
        }
    }
}
