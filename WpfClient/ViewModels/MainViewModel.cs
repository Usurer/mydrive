﻿using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using MyDrive.WpfClient.MVVM;
using MyDrive.WpfClient.Services;

namespace MyDrive.WpfClient.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private const string EnableSync = "Enable Sync Mode";
        private const string DisableSync = "Disable Sync Mode";

        public bool UseSyncMode;

        private readonly UploadController UploadController = new UploadController();
        private readonly DownloadController DownloadController = new DownloadController();

        private RemoteTreeViewModel _remoteTree = new RemoteTreeViewModel();
        private LocalTreeViewModel _localTree = new LocalTreeViewModel();
        private string _syncModeButtonText = EnableSync;

        private IFileDialogService FileDialogService = new FileDialogService();

        public ICommand LoadRemoteTreeNode { get; set; }
        public ICommand LoadLocalTreeNode { get; set; }

        public ICommand RefreshRemote { get; set; }
        public ICommand RefreshLocal { get; set; }

        public ICommand OpenLocalInSync { get; set; }
        public ICommand OpenRemoteInSync { get; set; }

        public ICommand UploadShallow { get; set; }
        public ICommand UploadRecursive { get; set; }
        public ICommand Download { get; set; }

        public ICommand ChangeSyncMode { get; set; }

        public ICommand ShowLocalRootDialog { get; set; }
        public ICommand Loaded { get; set; }

        public MainViewModel()
        {
            LoadRemoteTreeNode = new RelayCommand(async x =>
            {
                await RemoteTree.LoadTreeNodeAsync((NodeViewModel)x);
            });

            LoadLocalTreeNode = new RelayCommand(async x =>
            {
                await LocalTree.LoadTreeNodeAsync((NodeViewModel)x);
            });

            RefreshRemote = new RelayCommand(async x =>
            {
                await RemoteTree.LoadRoot();
            });

            RefreshLocal = new RelayCommand(x =>
            {
                LocalTree.LoadRoot();
            });

            OpenLocalInSync = new RelayCommand(async x =>
            {
                await LocalTree.OpenInSync(RemoteTree.SelectedNode ?? RemoteTree.RootNode);
            });

            OpenRemoteInSync = new RelayCommand(async x =>
            {
                await RemoteTree.OpenInSync(LocalTree.SelectedNode ?? LocalTree.RootNode);
            });

            UploadShallow = new RelayCommand(async x =>
            {
                await UploadSelection(false);
            });

            UploadRecursive = new RelayCommand(async x =>
            {
                await UploadSelection(true);
            });

            Download = new RelayCommand(x =>
            {
                DownloadSelection();
            });

            ChangeSyncMode = new RelayCommand(x =>
            {
                if (!UseSyncMode)
                {
                    UseSyncMode = true;
                    SyncModeButtonText = DisableSync;
                }
                else
                {
                    UseSyncMode = false;
                    SyncModeButtonText = EnableSync;
                }
            });

            Loaded = new RelayCommand(async x =>
            {
                async Task SetupRemote()
                {
                    await RemoteTree.LoadRoot();
                }
                var setupRemote = SetupRemote();

                ShowLocalRootDialog.Execute(null);
                await setupRemote;
            });

            ShowLocalRootDialog = new RelayCommand(x =>
            {
                var selectedPath = FileDialogService.ShowFileDialog();
                LocalTreeViewModel.LocalRoot = string.IsNullOrEmpty(selectedPath) ? LocalTreeViewModel.LocalRoot : selectedPath;
                LocalTree.LoadRoot();
            });

            RemoteTree.SelectedItemChanged += (sender, args) =>
            {
                if (UseSyncMode)
                {
                    OpenLocalInSync.Execute(null);
                }
            };

            LocalTree.SelectedItemChanged += (sender, args) =>
            {
                if (UseSyncMode)
                {
                    OpenRemoteInSync.Execute(null);
                }
            };
        }

        #region Properties

        public RemoteTreeViewModel RemoteTree
        {
            get => _remoteTree;
            set
            {
                _remoteTree = value;
                OnPropertyChanged(nameof(RemoteTree));
            }
        }

        public LocalTreeViewModel LocalTree
        {
            get => _localTree;
            set
            {
                _localTree = value;
                OnPropertyChanged(nameof(LocalTree));
            }
        }

        public string SyncModeButtonText {
            get => _syncModeButtonText;
            set
            {
                _syncModeButtonText = value;
                OnPropertyChanged(nameof(SyncModeButtonText));
            }
        }

        #endregion

        #region Upload/Download

        private async void DownloadSelection()
        {
            RemoteTree.Visibility = Visibility.Collapsed;
            RemoteTree.Progress.IsIndeterminate = true;
            RemoteTree.Progress.Visibility = Visibility.Visible;

            if (RemoteTree.SelectedNode.Value.IsFolder)
            {
                RemoteTree.Progress.IsIndeterminate = false;
                RemoteTree.Progress.Maximum = 100;

                // TODO: Using Dispatcher here seems to be a bad idea because it makes unit testing impossible and adds stright bounds between components.
                // See https://engy.us/blog/2010/03/31/using-the-dispatcher-with-mvvm/
                // Also https://msdn.microsoft.com/en-us/magazine/dn605875.aspx
                await DownloadController.DownloadFolderAsync(
                    (percentage) => { Application.Current.MainWindow.Dispatcher.Invoke(() => RemoteTree.Progress.Value = percentage); },
                    RemoteTree.SelectedNode.Model,
                    LocalTree.SelectedNode.Model);
            }
            else
            {
                await DownloadController.DonwloadFileAsync(RemoteTree.SelectedNode.Model, LocalTree.SelectedNode.Model);
            }

            LocalTree.LoadRoot();

            RemoteTree.Progress.IsIndeterminate = true;
            RemoteTree.Progress.Visibility = Visibility.Collapsed;
            RemoteTree.Visibility = Visibility.Visible;
        }

        private async Task UploadSelection(bool uploadSubfolders)
        {
            RemoteTree.Visibility = Visibility.Collapsed;
            RemoteTree.Progress.IsIndeterminate = true;
            RemoteTree.Progress.Visibility = Visibility.Visible;

            if (LocalTree.SelectedNode.Value.IsFolder)
            {
                await UploadController.UploadFolder(LocalTree.SelectedNode.Model, RemoteTree.SelectedNode.Model, uploadSubfolders);
            }
            else
            {
                RemoteTree.Progress.IsIndeterminate = false;
                RemoteTree.Progress.Maximum = 100;

                await UploadController.UploadFile(percentage => Application.Current.MainWindow.Dispatcher.Invoke(() => RemoteTree.Progress.Value = (int)percentage), RemoteTree.SelectedNode.Value.Id, LocalTree.SelectedNode.Value);
            }

            RemoteTree.Progress.IsIndeterminate = true;
            RemoteTree.Progress.Visibility = Visibility.Collapsed;
            RemoteTree.Visibility = Visibility.Visible;
        }

        #endregion
    }
}
