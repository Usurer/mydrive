﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Windows.Media;
using MyDrive.WpfClient.MVVM;
using MyDrive.WpfClient.ViewModels;

namespace MyDrive.WpfClient.Experiments
{
    internal class ExperimentalViewModel : ViewModelBase
    {
        public ExperimentalViewModel()
        {
            Root = new ObservableCollection<Leaf>
            {
                new Leaf
                {
                    Name = "Root",
                    Children = new ObservableCollection<Leaf>
                    {
                        new Leaf
                        {
                            Name = "Child",
                            Children = new ObservableCollection<Leaf>
                            {
                                new Leaf
                                {
                                    Name = "Child 2"
                                }
                            }
                        }
                    }
                }
            };
        }

        public ObservableCollection<Leaf> Root { get; set; }

        public Leaf _selectedItem = new Leaf();

        public Leaf SelectedItem
        {
            get
            {
                return _selectedItem;
            }
            set
            {
                _selectedItem = value;
                OnPropertyChanged(nameof(SelectedItem));
            }
        }
    }

    internal class Leaf : ViewModelBase
    {
        public Leaf()
        {
            ChangeColor = new RelayCommand((x) =>
            {
                ColorBrush.Color = ColorBrush.Color == Colors.Black
                    ? Colors.Red
                    : Colors.Black;
                Children.Add(new Leaf { Name = "a" });
                OnPropertyChanged(nameof(ColorBrush));
                //OnPropertyChanged(nameof(Children));

            });
            ColorBrush = new SolidColorBrush(Colors.Black);
        }

        public string Name { get; set; }

        public ObservableCollection<Leaf> Children { get; set; } = new ObservableCollection<Leaf>();

        public Leaf Parent { get; set; }

        public SolidColorBrush ColorBrush { get; set; }

        public ICommand ChangeColor { get; }
    }
}