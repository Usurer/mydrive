﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyDrive.Abstractions;

namespace MyDrive.LocalServices
{
    public class FilesService : IFilesService
    {
        public const string LocalRootFolder = @"C:\Coding\Meaningful Projects\MyDrive\ExampleRootFolder";

        public List<Node<IFile>> GetRootContents()
        {
            var root = new Node<IFile> { Value = new LocalFile { FullPath = LocalRootFolder } };
            return GetChildrenAsync(root).Result;
        }

        /// <summary>
        /// Isn't really async. Blocks the thread.
        /// </summary>
        public Task<List<Node<IFile>>> GetChildrenAsync(Node<IFile> file)
        {
            if (!file.Value.IsFolder)
            {
                throw new ArgumentException($"{file.Value.Id} is not a folder");
            }

            var root = file.Value.Id;
            var files = Directory.GetFiles(root).Select(x => new Node<IFile>
            {
                Value = new LocalFile { Name = Path.GetFileName(x), FullPath = x }, Parent = file
            });
            var folders = Directory.GetDirectories(root).Select(x => new Node<IFile>
            {
                Value = new LocalFile { Name = Path.GetFileName(x), FullPath = x, IsFolder = true }, Parent = file
            });

            return Task.FromResult(files.Concat(folders).ToList());
        }
    }
}
