﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MyDrive.Abstractions;

namespace MyDrive.LocalServices
{
    public class LocalFile : IFile
    {
        string IFile.Id
        {
            get => FullPath;
            set => FullPath = value;
        }

        public string Name { get; set; }

        public string FullPath { get; set; }

        public bool IsFolder { get; set; }
    }
}
