﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyDrive.Abstractions;
using MyDrive.Common.Tree;

namespace Tests
{
    [TestClass]
    public class TreeTests
    {
        private int CompareIntegers(int a, int b)
        {
            return a.CompareTo(b);
        }

        [TestMethod]
        public void TestIntersection_DifferentLists()
        {
            var list_1 = new List<int> {1, 2, 3};
            var list_2 = new List<int> {4, 5, 6};

            var result = TreeUtils.Intersect(list_1, list_2, CompareIntegers, i => i, i => i);

            Assert.IsTrue(result.LeftOnly.Count == 3);
            Assert.IsTrue(result.RightOnly.Count == 3);
            Assert.IsTrue(result.Common.Count == 0);

            Assert.AreEqual(1, result.LeftOnly.First());
            Assert.AreEqual(2, result.LeftOnly.Skip(1).First());
            Assert.AreEqual(3, result.LeftOnly.Skip(2).First());

            Assert.AreEqual(4, result.RightOnly.First());
            Assert.AreEqual(5, result.RightOnly.Skip(1).First());
            Assert.AreEqual(6, result.RightOnly.Skip(2).First());
        }

        [TestMethod]
        public void TestIntersection_OneIntersection()
        {
            var list_1 = new List<int> {3, 4, 5};
            var list_2 = new List<int> {1, 2, 3};

            var result = TreeUtils.Intersect(list_1, list_2, CompareIntegers, i => i, i => i);

            Assert.IsTrue(result.LeftOnly.Count == 2);
            Assert.IsTrue(result.RightOnly.Count == 2);
            Assert.IsTrue(result.Common.Count == 1);

            Assert.AreEqual(4, result.LeftOnly.First());
            Assert.AreEqual(5, result.LeftOnly.Skip(1).First());

            Assert.AreEqual(1, result.RightOnly.First());
            Assert.AreEqual(2, result.RightOnly.Skip(1).First());

            Assert.AreEqual((3, 3), result.Common.First());
        }

        [TestMethod]
        public void TestIntersection_TwoIntersections()
        {
            var list_1 = new List<int> {1, 2, 3};
            var list_2 = new List<int> {2, 3, 4};

            var result = TreeUtils.Intersect(list_1, list_2, CompareIntegers, i => i, i => i);

            Assert.IsTrue(result.LeftOnly.Count == 1);
            Assert.IsTrue(result.RightOnly.Count == 1);
            Assert.IsTrue(result.Common.Count == 2);

            Assert.AreEqual(1, result.LeftOnly.First());

            Assert.AreEqual(4, result.RightOnly.First());

            Assert.AreEqual((2, 2), result.Common.First());
            Assert.AreEqual((3, 3), result.Common.Skip(1).First());
        }

        [TestMethod]
        public void TestFind()
        {
            var r1 = new Node<int> {Value = 112};

            var list = new List<Node<int>>
            {
                new Node<int>
                {
                    Value = 100,
                    Children = new List<Node<int>>
                    {
                        new Node<int>
                        {
                            Value = 110,
                            Children = new List<Node<int>>
                            {
                                new Node<int> {Value = 111},
                                r1,
                                new Node<int> {Value = 113},
                            }
                        },
                        new Node<int> {Value = 120},
                        new Node<int> {Value = 130},
                    }},
                    new Node<int>
                    {
                        Value = 200,
                        Children = new List<Node<int>>
                        {
                            new Node<int> {Value = 210},
                            new Node<int> {Value = 220},
                            new Node<int> {Value = 230},
                        }},
                    new Node<int> {Value = 300},
            };

            var result = TreeUtils.Find(list, (x) => x.Value == 112);
            Assert.AreSame(r1, result);

            result = TreeUtils.Find(list, x => x.Value == 0);
            Assert.IsNull(result);

            result = TreeUtils.Find(list, (x) => x.Value == 100);
            Assert.AreSame(list.First(), result);
        }

        [TestMethod]
        public void TestFind_All()
        {
            var n_100 = new Node<int>
            {
                Value = 100,
                Children = new List<Node<int>>
                {
                    new Node<int> {Value = 120},
                    new Node<int> {Value = 130},
                }
            };

            var n_110 = new Node<int>
            {
                Value = 110,
                Children = new List<Node<int>>
                {
                    new Node<int> {Value = 111},
                    new Node<int> {Value = 113},
                }
            };
            n_110.AddChild(new Node<int> { Value = 112 });

            n_100.AddChild(n_110);

            var n_200 = new Node<int>
            {
                Value = 200,
                Children = new List<Node<int>>
                {
                    new Node<int> {Value = 210},
                    new Node<int> {Value = 230},
                }
            };

            n_200.AddChild(new Node<int> { Value = 112 });

            var list = new List<Node<int>>
            {
                n_100,
                n_200,
                new Node<int> {Value = 300},
            };

            var result = TreeUtils.FindAll(list, (x) => x.Value == 112);
            Assert.IsTrue(result.Count == 2);
            Assert.AreEqual(1, result.Count(x => x.Value == 112 && x.Parent.Value == 110));
            Assert.AreEqual(1, result.Count(x => x.Value == 112 && x.Parent.Value == 200));
        }

        [TestMethod]
        public void TestParentsSearch()
        {
            var root = new Node<int> { Value = 1 };
            var leaf_1_1 = new Node<int> { Value = 10, Parent = root };
            var leaf_1_2 = new Node<int> { Value = 15, Parent = root };

            var leaf_2_1 = new Node<int> { Value = 101, Parent = leaf_1_1 };
            var leaf_2_2 = new Node<int> { Value = 151, Parent = leaf_1_2 };

            var result = TreeUtils.GetAllParents(leaf_2_2).ToArray();
            Assert.AreEqual(2, result.Length);
            Assert.AreEqual(leaf_1_2, result[0]);
            Assert.AreEqual(root, result[1]);
        }
    }
}
