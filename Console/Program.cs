﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyDrive.GoogleServices;

namespace MyDrive.ConsoleClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.Unicode;

            // No need in async, since it's a Console app and await won't affect responsiveness
            var filesService = ServiceProvider.GetNewFilesService().Result;
            var rootContents = filesService.GetRootContentsAsync().Result;

            Console.WriteLine("Root files:");
            foreach (var node in rootContents.Children.Where(x => !x.Value.IsFolder))
            {
                Console.WriteLine(node.Value.Name);
            }


            Console.WriteLine($"{Environment.NewLine}Root folders:");
            foreach (var folder in rootContents.Children.Where(x => x.Value.IsFolder))
            {
                Console.WriteLine(Environment.NewLine + " " + folder.Value.Name + " contents:");
                var children = filesService.GetChildrenAsync(folder).Result;
                foreach (var child in children)
                {
                    Console.WriteLine(" " + child.Value.Name);
                }
            }
        }
    }
}
