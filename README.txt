A second attempt to implement a Google Drive client.
The reason behind doing it from scratch is that libraries, provided by Google, have changed much - thus it would take a lot of work to update the old code.
The second reason is that I want to move from Console client to WPF - simply because Console isn't very useful if I want an easy-to-use UI.