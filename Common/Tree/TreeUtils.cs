﻿using System;
using System.Collections.Generic;
using System.Linq;
using MyDrive.Abstractions;

namespace MyDrive.Common.Tree
{
    public static class TreeUtils
    {
        public static (List<T1> LeftOnly, List<T2> RightOnly, List<(T1, T2)> Common) Intersect<T1, T2>(List<T1> left, List<T2> right, Func<T1, T2, int> comparer, Func<T1, object> leftOrderSelector, Func<T2, object> rightOrderSelector)
        {
            var onlyInLeft = new List<T1>();
            var onlyInRight = new List<T2>();
            var common = new List<(T1, T2)>();

            var sortedLeft = left.OrderBy(leftOrderSelector).ToArray();
            var sortedRight = right.OrderBy(rightOrderSelector).ToArray();

            var leftIndex = 0;
            var rightIndex = 0;

            while (true)
            {
                var leftValue = sortedLeft[leftIndex];
                var rightValue = sortedRight[rightIndex];

                var comparison = comparer(leftValue, rightValue);

                if (comparison < 0)
                {
                    onlyInLeft.Add(leftValue);
                    leftIndex++;
                }
                else if (comparison > 0)
                {
                    onlyInRight.Add(rightValue);
                    rightIndex++;
                }
                else
                {
                    common.Add((leftValue, rightValue));
                    rightIndex++;
                    leftIndex++;
                }

                if (leftIndex == sortedLeft.Length)
                {
                    onlyInRight.AddRange(sortedRight.Skip(rightIndex));
                    break;
                }

                if (rightIndex == sortedRight.Length)
                {
                    onlyInLeft.AddRange(sortedLeft.Skip(leftIndex));
                    break;
                }
            }

            var result = (LeftOnly: onlyInLeft, RightOnly: onlyInRight, Common: common);
            return result;
        }

        public static Node<T> Find<T>(Node<T> item, Predicate<Node<T>> predicate)
        {
            if (predicate(item))
            {
                return item;
            }
            return Find(item.Children, predicate);
        }

        public static Node<T> Find<T>(List<Node<T>> list, Predicate<Node<T>> predicate)
        {
            var currentChild = list.Find(predicate);
            if (currentChild == null)
            {
                foreach (var node in list)
                {
                    var innerChild = Find<T>(node.Children, predicate);
                    if (innerChild != null)
                    {
                        return innerChild;
                    }
                }
            }
            return currentChild;
        }

        public static List<Node<T>> FindAll<T>(Node<T> item, Predicate<Node<T>> predicate)
        {
            var results = new List<Node<T>>();
            if (predicate(item))
            {
                results.Add(item);
            }

            results.AddRange(FindAll(item.Children, predicate));
            return results;
        }

        public static List<Node<T>> FindAll<T>(List<Node<T>> list, Predicate<Node<T>> predicate)
        {
            var results = list.FindAll(predicate);
            foreach (var node in list)
            {
                results.AddRange(FindAll<T>(node.Children, predicate));
            }

            return results;
        }

        /// <summary>
        /// Returns all ancestors of current Node starting from the closest parent and ending with a root of the tree.
        /// Since List.Add does insert an item to the and of the list, we'll have a proper Parents order here.
        /// </summary>
        public static List<Node<T>> GetAllParents<T>(Node<T> item)
        {
            var result = new List<Node<T>>();

            while (item.Parent != null)
            {
                result.Add(item.Parent);
                item = item.Parent;
            }

            return result;
        }
    }
}
