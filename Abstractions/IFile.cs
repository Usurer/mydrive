﻿namespace MyDrive.Abstractions
{
    public interface IFile
    {
        string Id { get; set; }

        string Name { get; set; }
        
        bool IsFolder { get; set; }
    }
}
