﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDrive.Abstractions
{
    public interface IFilesService
    {
        Task<List<Node<IFile>>> GetChildrenAsync(Node<IFile> file);
    }
}
