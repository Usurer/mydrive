﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyDrive.Abstractions;

namespace MyDrive.Abstractions
{
    public class Node<T>
    {
        public T Value { get; set; }

        public List<Node<T>> Children { get; set; } = new List<Node<T>>();

        public Node<T> Parent { get; set; }

        public void AddChild(Node<T> child)
        {
            Children.Add(child);
            child.Parent = this;
        }
    }
}
