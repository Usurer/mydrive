﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyDrive.Abstractions;

namespace MyDrive.GoogleServices
{
    public class RemoteFile : IFile
    {
        public string Name { get; set; }

        public string Id { get; set; }

        public bool IsFolder { get; set; }
    }
}
