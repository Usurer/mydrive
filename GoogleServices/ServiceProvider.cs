﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDrive.GoogleServices
{
    public static class ServiceProvider
    {
        public static async Task<FilesService> GetNewFilesService()
        {
            var credential = await new AuthorizationService().AuthorizeAsync();
            var service = new FilesService(credential);
            return service;
        }
    }
}
