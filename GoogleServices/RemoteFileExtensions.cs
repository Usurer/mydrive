﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Google.Apis.Drive.v3.Data;

namespace MyDrive.GoogleServices
{
    internal static class RemoteFileExtensions
    {
        public static RemoteFile FromFile(this File file)
        {
            return new RemoteFile
            {
                Id = file.Id,
                Name = file.Name,
                IsFolder = file.MimeType.Equals(MimeTypes.FolderMimeType, StringComparison.OrdinalIgnoreCase),
            };
        }
    }
}
