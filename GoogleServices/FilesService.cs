﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Requests;
using Google.Apis.Upload;
using MyDrive.Abstractions;
using File = Google.Apis.Drive.v3.Data.File;

namespace MyDrive.GoogleServices
{
    public class FilesService : IFilesService
    {
        private static DriveService Service { get; set; }

        private BatchRequest NewBatchRequest => new BatchRequest(Service);

        public event EventHandler<long> UploadProgressChanged;
        public event EventHandler UploadFinished;

        internal FilesService(UserCredential userCredential)
        {
            Service = new DriveService(new Google.Apis.Services.BaseClientService.Initializer
            {
                ApplicationName = "My Drive",
                HttpClientInitializer = userCredential,
            });
        }

        public async Task<IEnumerable<string>> ListFiles()
        {
            var request = Service.Files.List();
            var files = await request.ExecuteAsync();

            return files.Files.Select(x => x.Name).ToArray();
        }

        /// <summary>
        /// BUG: Doesn't set parent value to found children
        /// </summary>
        /// <returns></returns>
        public async Task<Node<IFile>> GetRootContentsAsync()
        {
            var request = new FilesResource.GetRequest(Service, "root");
            var root = await request.ExecuteAsync();

            var contents = await GetAllFilesByParentIdAsync(root.Id);
            var result = new Node<IFile>
            {
                Value = root.FromFile(),
                Children = contents.Select(x => new Node<IFile> { Value = x.FromFile() }).ToList(),
                Parent = null, // Root has no parent
            };
            return result;
        }

        public async Task<List<Node<IFile>>> GetChildrenAsync(Node<IFile> parent)
        {
            var children = await GetAllFilesByParentIdAsync(parent.Value.Id);
            var result = children.Select(x => new Node<IFile>
            {
                Value = x.FromFile(),
                Parent = parent,
            }).ToList();
            return result;
        }

        private async Task<List<File>> GetAllFilesByParentIdAsync(string parentId)
        {
            var files = new List<File>();
            var request = new FilesResource.ListRequest(Service)
            {
                Q = $"'{parentId}' in parents",
                PageSize = 10, // default value is 100 and looks good to me, but for testing purposes I'll make it smaller - this may help to find bugs
            };
            
            var response = await request.ExecuteAsync();
            files.AddRange(response.Files);

            while (!string.IsNullOrEmpty(response.NextPageToken))
            {
                request.PageToken = response.NextPageToken;
                response = await request.ExecuteAsync();
                files.AddRange(response.Files);
            }

            return files;
        }

        public async Task UploadFileAsync(string fileName, Stream fileStream, string mimeType, string parentId)
        {
            // https://developers.google.com/drive/api/v3/manage-uploads
            // It seems that Google can handle an empty string as MIME type.
            // CreateMediaUpload does inherit ResumableUpload, so there's no need to invent something else.
            var request = new FilesResource.CreateMediaUpload(Service, new File { Name = fileName, Parents = new List<string> { parentId }}, fileStream, mimeType);
            request.ProgressChanged += progress =>
            {
                var len = fileStream.Length;
                if (progress.Status == UploadStatus.Uploading && len > 0)
                {
                    var sent = 100 * progress.BytesSent / len;
                    UploadProgressChanged?.Invoke(this, sent);
                }
                else if (progress.Status == UploadStatus.Completed)
                {
                    UploadFinished?.Invoke(this, null);
                }
            };
            await request.UploadAsync();
        }

        public async Task DownloadFileAsync(IFile file, FileStream target)
        {
            var request = new FilesResource.GetRequest(Service, file.Id);
            await request.DownloadAsync(target);
        }

        public async Task<RemoteFile> CreateFolderAsync(string name, string parentId = "root")
        {
            var fileMetadata = new File()
            {
                Name = name,
                MimeType = "application/vnd.google-apps.folder",
                Parents = new List<string> { parentId },
            };
            var request = Service.Files.Create(fileMetadata);

            request.Fields = "id, name, mimeType";

            var file = await request.ExecuteAsync();
            return file.FromFile();
        }

        public async Task<string> GetParentIdAsync(string fileId)
        {
            var request = new FilesResource.GetRequest(Service, fileId);
            request.Fields = "parents";
            var result = (await request.ExecuteAsync());

            return result.Parents.SingleOrDefault(); ;
        }
    }
}
