﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MyDrive.GoogleServices
{
    internal class AuthorizationService
    {
        public async Task<UserCredential> AuthorizeAsync()
        {
            using (var stream = new FileStream("client_secret.json", FileMode.Open))
            {
                var secrets = GoogleClientSecrets.Load(stream).Secrets;
                return await GoogleWebAuthorizationBroker.AuthorizeAsync(secrets, new[] { DriveService.Scope.Drive }, "user", CancellationToken.None);
            }
        }
    }
}
